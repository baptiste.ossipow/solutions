# Life timeline

A Pen created on CodePen.io. Original URL: [https://codepen.io/slatiner/pen/Gmxle](https://codepen.io/slatiner/pen/Gmxle).

This is a little life and work timeline project I was working on. Made with Fontello icon fonts, a filter navigation system, a bit of jquery and a bunch of CSS3 features.
Feel free to use the 'J' and 'K' keys for navigation.

Will feel much better on full screen.